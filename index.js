function getKeysWithHighestValue(o, n, top){
    var keys = Object.keys(o);
    var vals = Object.values(o)
    keys.sort(function(a,b){
        return o[b] - o[a];
    })

    var top10Data = []

    if (top) {
        var topKeys = keys.slice(0,n);
        // get name from object matching 'key'
        var topVals = topKeys.map(key => o[key]) //vals.slice(0, n);
    
        for (var i = 0; i < topKeys.length; i++) {
            top10Data.push({'name': topKeys[i], 'value': topVals[i]})
        }
    } else {
        var lowKeys = keys.slice(Math.max(keys.length - n, 0));
        // get name from object matching 'key'
        var lowVals = lowKeys.map(key => o[key]) //vals.slice(0, n); 

        for (var i = 0; i < lowKeys.length; i++) {
            top10Data.push({'name': lowKeys[i], 'value': lowVals[i]})
        }
    }

    return top10Data
}

function createSVG (id, csvData, top, width, height) {
    let svg;

        svg = d3.select(id)
            .append('svg')
            .attr('height', height)
            .attr('width', width)
            .attr('viewBox', [0, 0, width, height])

    // our data
    // Get the unique suburb names
    const uniqueVals = new Set()
        for (const item of csvData) {
            uniqueVals.add(item.key)
    }
    // create an object li with suburbs plus totals like "SUBIACO": 4455, "PERTH": 7575
    const typeAndNumber = csvData.reduce((r,d)=>{
        if (!r[d.key]) {r[d.key]=0}
        r[d.key]+=parseInt(d.TotalAnnual)
        return r
    }, {})

    // data for the TOP 10 chart
    let data;
    if (top) {
        data = getKeysWithHighestValue(typeAndNumber, 10, top)
    } else {
        data = getKeysWithHighestValue(typeAndNumber, 10, top)
    }

    // Value Accessors and more vars defined
    const xValue = d => d.name;
    const yValue = d => d.value;

    const margin = { top: 20, right: 10, bottom: 70, left: 50 }
    const innerWidth = width - margin.left - margin.right;
    const innerHeight = height - margin.top - margin.bottom;
    let color;

    if (top) {
        color = 'red'
    } else {
        color = "#6dc388"
    }

    // scale x axis, with range as num of items in array
    const x = d3.scaleBand()
            .domain(data.map(xValue)) // x.domain() output = [0, 1, 2]
            .range([0, innerHeight]) // [0, 400] output = [0, 400]
            .padding(0.1);
    
    const y = d3.scaleLinear()
        .domain([0, d3.max(data, yValue)])
        .range([innerWidth - 50, 0]);

    // Top 10 Map       
    const g = svg.append('g')
    .attr('transform', `translate(${margin.left}, ${margin.top})`)
    
    g.append('g').call(d3.axisLeft(y))
    var gXAxis = g.append('g').call(d3.axisBottom(x))
        .attr('transform', `translate(0, ${innerHeight})`) // to put xAxis at the bottom
    
    gXAxis.selectAll("text") // this and below formats text position, x axis
        .attr("class", "x-axis-text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", "rotate(-65)")
    
    g
        .selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
            .attr('x', (d) => 
                x(xValue(d))
            ) // you need this as well, writing this made the bars not overlap anymore
            .attr('y', (d) => 
                y(yValue(d))
            )
            .attr('height', d => y(0) - y(yValue(d)) - 80)
            .attr('width', x.bandwidth()) // computed width of a single bar
            .attr('fill', color)

    return svg.node()
}


function highestAndLowest(data, region){
    let filtersArray = ["PERTH", "MELALEUCA"]
    filtersArray.push(region)

    // relevant data for all 3 suburbs
    const dataForSuburbs = data.filter(item => {
        return filtersArray.indexOf(item.key) > -1;
    })

    let finalArray = [];

    filtersArray.forEach(function(area) {
        let tempArray = [];
        // our data
        // Get the unique names
        const uniqueVals = new Set()
            for (const item of dataForSuburbs) {
                uniqueVals.add(item.Offence)
        }

        const yourRegionData = dataForSuburbs.filter(item => {
            return item.key === area;
        })

        // create an object li with suburbs plus totals like "ARSON": 100
        const regionObject = yourRegionData.reduce((r,d)=>{
            if (!r[d.Offence]) {r[d.Offence]=0}
            // parseInt d.value2 makes everything equal zero, so i removed this
            // such small values i timesd by 100
            r[d.Offence]+=(d.value2 * 100)
            return r
        }, {})
            
        for (var key of Object.keys(regionObject)) {
            tempArray.push({'area': area, 'name': key, 'value': regionObject[key]})
        }

        tempArray.sort(function(a, b) {
            var textA = a.name
            var textB = b.name
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        for (var i = 0; i < tempArray.length; i++) {
            finalArray.push(tempArray[i]);
        }
    })
    return finalArray;
}

function createLine(id, csvData, width, height) {

    // create a set of suburb names from csvData
    const uniqueVals = new Set()
    for (const item of csvData) {
        uniqueVals.add(item.key)
    }

    // from set of suburbs, convert to array of suburbs
    const suburbArray = Array.from(uniqueVals)

    // add the options to the button
    d3.select("#selectButton")
        .selectAll('myOptions')
            .data(suburbArray)
        .enter()
            .append('option')
        .text(function (d) { 
            return d; 
            }) // text showed in the menu
        .attr("value", function (d) { 
            return d;
            }) // corresponding value returned by the button

    var data = highestAndLowest(csvData, "BURNS BEACH")

    var svg = d3.select(id)
        .append('svg')
        .attr('height', height)
        .attr('width', width)
        .attr('viewBox', [0, 0, width, height])

    var margins = {
        top: 20,
        right: 20,
        bottom: 120,
        left: 50
    }
    // nests the data - crime type totals by suburb.
    var dataGroup = d3.nest()
        .key(function(d) {
            return d.area;
        })
        .entries(data)

    // axes scales: min and max data values in domain, vs pixel range in range
    var xScale = d3.scalePoint().range([margins.left, width - margins.right]).domain(data.map(function(d) { return d.name; }));

    var yScale = d3.scaleLinear().range([(height - 100) - margins.top, margins.bottom]).domain(d3.extent(data, function (d) {
        return parseInt(d.value);//or jelly etc..
    }))

    // Usually you have a color scale in your chart already
    var colorScale = d3.scaleOrdinal(d3.schemeCategory10)

    var g = svg.append('g')
    // .attr('transform', `translate(${margins.left}, ${margins.top})`)
    
  
    g.append('g').call(d3.axisLeft(yScale))
        .attr("transform", "translate(" + (margins.left) + ",0)")
    var gXAxis = g.append('g').call(d3.axisBottom(xScale))
        .attr('transform', `translate(0, ${height - margins.bottom})`)

    gXAxis.selectAll("text") // this and below formats text position, x axis
        .attr("class", "x-axis-text")
        .style("text-anchor", "start")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", "rotate(65) translate(15, 5)")
    
    var lineGen = d3.line()
        .x(function(d) { 
            return xScale(d.name); 
        })
        .y(function(d) { 
            return yScale(parseInt(d.value)); 
        })

    var counter = 0;
    dataGroup.forEach(function(d, counter, i) {
        svg.append('path')
            .attr('class', 'line')
            .attr('id', "id"+counter)
            .attr('d', lineGen(d.values))
            .attr('stroke', function(){
                return d.color = colorScale(d.key)
            })
            .attr('stroke-width', 2)
            .attr('fill', 'none');

         svg.append("text")
            .attr('class', 'text-index')
            .attr('id', "textid"+counter)
            .attr("x", 350)
            .attr("y", function(d, i){ 
                return 120 + counter*25}
            ) // 100 is where the first dot appears. 25 is the distance between dots  
            .style("fill", function(){ return d.color = colorScale(d.key)})
            .text(d.key)

        counter += 100;
    });


        // A function that update the chart
        function update(selectedGroup) {

            // Create new data with the selection?
            var dataFilter = csvData.filter(function(d){
                return d.key==selectedGroup
            })

            var data = highestAndLowest(csvData, selectedGroup)
            // nests the data - crime type totals by suburb.
            var dataGroup = d3.nest()
            .key(function(d) {
                return d.area;
            })
            .entries(data)
            
            var yScale = d3.scaleLinear().range([(height - 100) - margins.top, margins.bottom]).domain(d3.extent(data, function (d) {
                return parseInt(d.value);//or jelly etc..
            }))

            var lineGen = d3.line()
                .x(function(d) { 
                    return xScale(d.name); 
                })
                .y(function(d) { 
                    return yScale(parseInt(d.value)); 
                })

            // Select the section we want to apply our changes to
            var svg = d3.select("body").transition();
            // Make the changes
            dataGroup.forEach(function(d, counter, i) {
                if (counter === 2) {
                    svg.select('#id2') 
                        .duration(750)
                        .attr('d', lineGen(d.values))
                        .attr('stroke', function(){
                            return d.color = colorScale(d.key)
                        })
                        .attr('stroke-width', 2)
                        .attr('fill', 'none');

                    svg.select("#textid2")
                        .duration(750)
                        .attr("x", 350)
                        .attr("y", function(d, i){ 
                            return 120 + counter*25}
                        ) // 100 is where the first dot appears. 25 is the distance between dots  
                        .style("fill", function(){ return d.color = colorScale(d.key)})
                        .text(d.key)
                }
            });

        }

        // When the button is changed, run the updateChart function
        d3.select("#selectButton").on("change", function(d) {
            // recover the option that has been chosen
            var selectedOption = d3.select(this).property("value")
            // run the updateChart function with this selected option
            update(selectedOption)
        })
}