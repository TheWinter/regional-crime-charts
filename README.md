## About
Project charting Perth Crime data using D3, DC, Crossfilter, and Leaflet

## Issues
This is a beginner project with the aim of learning the technologies listed above. It is a work in progress, with the following things needing work:
 - Change the line chart into a grouped bar chart
 - Fix bug on choropleth map causing colors not to show properly
 - fix the chart associated with choropleth chart, which atm is broken

![Alt text](/images/line.png?raw=true)

![Alt text](/images/highest.png?raw=true)

![Alt text](/images/choropleth.png?raw=true)




